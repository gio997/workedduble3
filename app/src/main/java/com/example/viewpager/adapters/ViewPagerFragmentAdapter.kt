package com.example.viewpager.adapters

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.viewpager.fragment.FirstFragment
import com.example.viewpager.fragment.SecondFragment

class ViewPagerFragmentAdapter(activity: AppCompatActivity) : FragmentStateAdapter(activity) {
    override fun getItemCount() = 2

    override fun createFragment(position: Int): Fragment {

        if (position ==1) {
            return FirstFragment()

        }
            return SecondFragment()

    }
}